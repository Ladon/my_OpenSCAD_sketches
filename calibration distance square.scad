$fn=100;
linear_extrude(height = 2)
difference() {
    polygon(points = [[-50,0],[-50,25],[35,100],[50,100],[50,0]]);
    {
        polygon(points = [[35,15],[-35,15],[-35,25],[35,85]]);
        for(i = [-40:10:40])
            translate([i,0])
                cm();
        for(i = [-48:2:48])
            if (i % 10 != 0)
                translate([i,0])
                    mm();
        translate([50,50]) rotate([0,0,90]) {
            for(i = [-30:10:40])
                translate([i,0])
                    cm();
            translate([-40,0]) mm();
            for(i = [-42:2:48])
                if (i % 10 != 0)
                    translate([i,0])
                        mm();
        }
        rotate([0,0,-139.3])
        translate([-85,-54])
        for( i = [0:100]) {
            if (i % 10 == 0) {
                translate([i,0])
                    cm();
            } else if (i % 2 == 0) {
                translate([i,0])
                    mm();
            }
        }
    }
}
module cm() {
    l=10;
    translate([0,l/2]) square([0.5,l],center=true);
}
module mm() {
    l=5;
    translate([0,l/2]) square([0.5,l],center=true);
}