$fn = 100; // raising detail

x_scale = 100/101.1;
y_scale = 100/101.55;

h = 0.7; // height
w = 2; // width
l = 100; // length

scale([x_scale,y_scale,1])
	rod(l + 4);
rotate([0,0,90])
	scale([x_scale,y_scale,1])
		rod(l + 4);
for (i = [1:2:5]) {
	scale([x_scale,y_scale,1])
		ring(i * l / 5);
}
	
module rod(l = l) { // length (l) x width (w) x height (h)
	translate([0,0,h/2]) { // raise
		cube([l,w,h],center=true);
	}
}

module ring(d = l) {
	linear_extrude(height = h) {
		difference() {
			circle(d = d);      //   larger disc
			circle(d = d- w); // - smaller disc
		}
	}
}

