$fs = 0.06;
$fa = 0.1;
extra_w = 0.6;
extra_l = extra_w;
w = 12.9 + extra_w;
l = 20.4 + extra_l;
perimeter = 1;
epoxy_h = 1.7;
h_with_epoxy = 4.5;
extra_h = 1.3;
extra_space = 0.2;
outer_cube_w = w + 2 * perimeter;
outer_cube_l = l + 2 * perimeter;
outer_cube_h = h_with_epoxy + perimeter + extra_h + extra_space;
inner_cube_h = h_with_epoxy + extra_h + extra_space;
inner_cube_w = w;
inner_cube_l = l;

// the board with the elements //
translate([perimeter,perimeter,perimeter])
	%cube([inner_cube_l,inner_cube_w,inner_cube_h - extra_h]);
// the actual box //
difference() {
		color("yellow")
		cube([outer_cube_l,outer_cube_w,outer_cube_h]);
		color("navy")
		translate([perimeter,perimeter,perimeter])
			cube([inner_cube_l,inner_cube_w,inner_cube_h]);
}
// the locking clips //
//clips_r = 1;
clips_l = 3;
clips_w = 0.5;
clips_h = extra_h;
module clip() {
	scale([clips_l,clips_w,clips_h])
		rotate([0,90,0])
			cylinder(d = 1,center= true);
}
// 1st //
translate([outer_cube_l / 3,perimeter,outer_cube_h - clips_h / 2])
	clip();
// 2nd //
translate([2 * outer_cube_l / 3,perimeter,outer_cube_h - clips_h / 2])
	clip();
// 3rd //
translate([outer_cube_l / 3,perimeter + inner_cube_w,outer_cube_h - clips_h / 2])
	clip();
// 4th //
translate([2 * outer_cube_l / 3,perimeter + inner_cube_w,outer_cube_h - clips_h / 2])
	clip();
