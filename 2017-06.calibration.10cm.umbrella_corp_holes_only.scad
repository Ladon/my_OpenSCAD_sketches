$fs = 0.1;
module disc() {
	radious = 2;
	w = 1.7;
	difference() {
		circle(r = radious);
		circle(r = radious - w);
	}
}
steps = 360 / 45;
linear_extrude(height = 0.25) {
	disc();
	for ( i = [0 : 360 / steps : 360])
		rotate([0,0,i])
			for (l = [25,50])
				translate([l,0,0])
					disc();
}