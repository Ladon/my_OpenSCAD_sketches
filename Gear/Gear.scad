//$fn = 100;
$fs = 0.1;
$fa = 1;
// Parameters (mm):
// ----------------
teeth = 13;
// Maximum radius:
// ---------------
R = 10;
// Teeth's amplitude:
// ------------------
A = 10 / 100 * R;
hole_r = min(R - 3 * A, 10 / 2);
// Height:
// -------
H = 10;
// Code:
// -----
function A(a) = R - A * cos(teeth * a);
function coords(a) =
[
	A(a) * sin(a), 
	A(a) * cos(a)
];
difference()
{
	linear_extrude(height = H, twist = 45)
	polygon
	([
		for (a = [0: 1: 360]) 
			coords(a)
	]);
	translate([0,0,-1])
	cylinder(h = H + 2, r = hole_r);
}