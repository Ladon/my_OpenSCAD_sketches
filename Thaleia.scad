// detail
$fn = 100;

module triangle() {
	base_tri_points = [
		[0,sqrt(3)], // isoplevro plevras '2'
		[1,0],
		[-1,0]];
	polygon(base_tri_points);
}
module rod() {
	height = 5;
	rod_points = [
		[height,1,0],	// 0
		[height,-1,0],	// 1
		[height + sqrt(3),0,0],	// 2
		[0,1,height],			// 3
		[0,-1,height],			// 4
		[0, 0, height + sqrt(3)]	// 5
		];
	rod_faces = [
		[0,2,1],
		[2,0,3,5],
		[1,2,5,4],
		[0,1,4,3],
		[3,4,5]
		];
	polyhedron(rod_points, rod_faces);
	mini_rod_points = [
		[height,1,0],	// 0
		[height,-1,0],	// 1
		[height + sqrt(3),0,0],	// 2
		[-1/sqrt(3),1,2.59],	// 3
		[-1/sqrt(3),-1,2.59],	// 4
		[sqrt(3)-1/sqrt(3),0,2.59]	// 5
		];
	mini_rod_faces = [
		[0,2,1],
		[3,4,5],
		[2,0,3,5],
		[1,2,5,4],
		[0,1,4,3]
		];
	polyhedron(mini_rod_points, mini_rod_faces);
}
many = 3;
for (i = [0: many - 1])
	rotate([0,0,360 / many * i])
		rod();
diameter = 1;
translate([0,0,2.59 + diameter / 2])
	sphere(d=diameter);