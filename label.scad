length = 50;
height = 40;
width = 3;
module label(length = length, height = height)
{
	module main()
	{
		extra_width = 3;
		color("olive")
		linear_extrude(height = width,center = true)
		difference()
		{
			square([length + extra_width,height], center = true);
			// crop the top //
			// add more to the bottom //
			translate([0,width])
			square([length - width,height - width], center = true);
		}
	}
	module foot(length = 7 / 4 * length)
	{
		module part()
		{
			width = 4;
			rotate([180,0])
			polygon([[-width / 2,0],[width / 2,0],[0,length]]);
		}
		translate([0,0,width / 2])
		rotate([0,180,0])
		linear_extrude(height = width / 2, scale = [10 / 4,1])
		part();
		translate([0,0,-width / 2])
		linear_extrude(height = width / 2, scale = [10 / 4,1])
		part();
		/* original code
	module foot(height = 5 / 4 * length)
	{
		length = 10;
		module part()
		{
			rotate([180,0])
			polygon([[-length / 4,0],[length / 4,0],[0,height]]);
		}
		linear_extrude(height = width / 2, scale = 0)
		part();
		rotate([0,180,0])
		linear_extrude(height = width / 2, scale = 0)
		part();
	}
		original code */
	}
	
	main();
	translate([0,-height / 2])
	foot();
}

// double pyramid (up + down) //
module ruby()
{
	polyhedron( 
	points = 
		[[0,0,width / 2],				// 0
		[width / 2, width / 2, 0],		// 1
		[width / 2, -width / 2, 0],		// 2
		[-width / 2, -width / 2, 0],	// 3
		[-width / 2, width / 2, 0],		// 4
		[0,0,-width / 2]],
	faces = 
		[[0,1,2],
		[0,2,3],
		[0,3,4],
		[0,4,1],
		[5,1,4],
		[5,4,3],
		[5,3,2],
		[5,2,1]]
	);
}

difference()
{
	label();
	// πλευρικές εσοχές //
	translate([0,width,0])
	rotate([0,45,0])
	cube([length / sqrt(2), height - width, length / sqrt(2)], center = true);
	// bottom εσοχή //
	translate([0,-(height - width) / 2 + width,0])
	{
		rotate([45,0,0])
		cube([length - width, width / sqrt(2), width / sqrt(2)],center=true);
		// γωνίες //
		translate([(length - width) / 2,0,0])
		ruby();
		translate([-(length - width) / 2,0,0])
		ruby();
	}
}

loose=1.7;
translate([0,width/2+loose/4,0])
cube([length-loose,height-width-loose/2,1],center=true);