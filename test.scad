$fs = 0.1; // minimum fragment size in mm (wiki)
$fa = 8; // minimum angle for a fragment (i suppose 3 ~ max 120 fragments/circle)
//$fn = 300; // ignore others. is parts per circle
/* sorry, my printer needs scaling, currently.
 */ 
//x_scale = 100/101.1;
//y_scale = 100/101.55;
x_scale = 100/101;
y_scale = 100/101;
//x_scale = 0.99;
//y_scale = 0.99;
x_scale = 1;
y_scale = 1;
/* */
gl = 108.75; // epoxy length
gw = 53.35; // epoxy width
sh = 8.6;	    // height above epoxy
eh = 1.6;   // epoxy height
ph = 1.75;   // pin height
gh = sh + eh + ph; //
empty_space = 0.3; // for all holes
perimeter_w = 5; // at around 5.7 from top is a component near the capacitor
ceiling_w = min(perimeter_w, 2); // 2nd is arbitrary
module feet() {
	echo(fh);
	fh = gh + 2;  // pillars will reach 2mm below pins
	fd = 3; // foot diameter (hole diameter) // fixed
	fbd = fd * 3; // base of cylinders (arbitrary)
	fbh = sh - ceiling_w - 0; // height of bases, 0mm above epoxy (arbitrary)
	bd = fd + 2 * 0.125; // ball diameter
	translate([0,0,ceiling_w + fbh])
		rotate([0,180,0]) {
			foot(0,0);
			ball(0,0);
		}
	// modules //
	module foot(x,y) {
		translate([fd / 2, fd / 2, 0]) {
			translate([x, y, -(fh - sh)])
				cylinder(d =  fd,h = fh);
		}
	}
	module base(x, y) {
		translate([fd / 2, fd / 2, 0]) 
			translate([x, y, sh])
				mirror([0,0,1])
					linear_extrude(height = fbh + ceiling_w, scale = fd / fbd)
						circle(d = fbd);
	}
	module ball (x, y) {
		translate([fd / 2, fd / 2, 0])
			translate([x, y, -eh - 0.5])
				sphere(d = bd);
	}
}
scale([x_scale,y_scale,1])
	feet();