$fs=0.5;
base_h = 40;
base_w = 130;
base_perim_w = 2;
// box //
difference() {
	// outer //
	color("Magenta")
	cube([base_w,base_w,base_h]);
	// inner //
	translate([base_perim_w/2, base_perim_w/2,base_perim_w/2])
	color("Fuchsia")
	cube([base_w - base_perim_w, base_w - base_perim_w, base_h - base_perim_w/2]);
}
// 2x rings //
ring_h = 2;
ring_w = 4;
ring_spacing = 35;
wing_width = 0.1;
wing_length = 20;
main_cylinder_d = wing_width*wing_length * 5;
extra_space = 0.3;

ring_pos = 20;

module ring() {
	difference() {
		cylinder(d = main_cylinder_d + 2 * ring_w,h = ring_h);
		cylinder(d = main_cylinder_d,h = ring_h);
	}
}
ring_holder_w = 1;
ring_holder_num = 15;
module ring_holder() {
	cube([ring_holder_w,ring_holder_w,base_h + ring_h + ring_pos + ring_spacing + ring_h]);
}
translate([base_w/2, base_w/2,0]) {
/* ring holder *//*
	for (i = [0:360/ring_holder_num:360])
		rotate([0,0,i])
		translate([main_cylinder_d/2 + ring_w - ring_holder_w,0,0])
		ring_holder();
*/
	translate([0,0,base_h + ring_h + ring_pos]) {
		ring();
		translate([0,0,ring_spacing])
		ring();
	}
}

ring_holder_d = ring_w;
ring_holder_num_circular = 7;
ring_holder_circular_scale_factor= 0.5;
ring_holder_circular_extra_pos = 10;
ring_holder_circular_pos=main_cylinder_d/2 + ring_holder_d/2+ring_holder_circular_extra_pos;
ring_holder_circular_twist = 360;

d = ring_holder_d;
n = ring_holder_num_circular;
sf = ring_holder_circular_scale_factor;
pex = ring_holder_circular_extra_pos;
p = ring_holder_circular_pos;
t = ring_holder_circular_twist;

translate([base_w/2,base_w/2,0])
for (i = [0:360/n:360])
	rotate([0,0,i])
	translate([(p - (ring_w-d/2))*(1-sf)-pex,0,0])
	color("yellow")
	linear_extrude(height=ring_pos + base_h + ring_h + ring_h,scale=sf,twist=t)
	translate([p,0,0])
	circle(d=d);

a = -45;
pos = -100;
s = 0.8;
translate([(1-s) * pos * cos(a),-(1-s) * pos * sin(a),-30])
linear_extrude(height=10,twist=a,scale=s)
translate([pos,0])
circle(d = 10);