// the following are measured by hand(tool)
// holes    : 3mm
// width    : 53.4mm
// length   : 108.8mm
// hole perimeter to width, down : 1mm
// hole perimeter to width, up   : 1mm
// hole perimeter to length      : 12.3mm
// 4 holes, inner distance, width   : 24mm
// 4 holes, inner distance, height  : 24.85mm
// 4 holes, distance to bottom      : 6.05mm
// 4 holes, distance from top       : 16.25mm
// 4 holes, distance to right side  : 14mm
// 4 holes, distance to left side   : 64.5mm
// epoxy width  : 1.6mm
// pins approx  : 1.65mm (eye)
/*  
 * ~< H E I G H T s >~ (taken WITH epoxy height)
 * pins(colored)    : 10.05 <- 10.1 -> 10.25
 * sd card          : 3.55
 * power plug       : 9.05
 * usb              : 4.2 to 4.4
 * reset (button)   : 5.15
 * upgrade (button) : 5.1
 * power (button)   : 5.15
 * jtag (approx)    : 10.3
 * antenna          : 3.5
 * antenna (width)  : 10.15
 * 3x audio         : 6.85
 * 3x audio (width) : 6.15
 * 1x phone         : 6.35
 * 1x phone (width) : 6.55
 */
width=53.4;
length=108.8;
epoxy_width=1.6;
pins_below=1.65;
difference() {
    /*
     * Base
     */
    color("yellow")
    translate([-1,-1,-1])
    // ^ center is on actual epoxy board, not the cover's perimeter
    cube([length+2,width+2,epoxy_width+1+pins_below],center=false); 
    // ^ +1mm on each side
    color("blue") cube([length,width,epoxy_width+pins_below+10],center=false);
}
diameter=3;
// ^ hole
radius=1.5;
for(x = [
        [12.3+radius,1+radius,0],
        [12.3+radius,width-1-radius,0],
        [64.5+radius,6.05+radius,0],
        [64.5+radius,width-16.25-radius,0],
        [length-14-radius,6.05+radius,0],
        [length-14-radius,width-16.25-radius,0]
        ]){
    /*
     * Base (holes)
     */
    translate(x) {
        difference() {
            color("red")
            cylinder(r=radius+1,h=pins_below,$fn=50);
            // ^ 1mm foot for the board to stand
//           translate([0,0,pins_below])
            color("yellow")
            cylinder(r=radius,h=pins_below,$fn=50);
        }
    }
}
