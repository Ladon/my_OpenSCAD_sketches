module rod() {
	translate([-50,-0.25/2,0])
		cube([100,0.25,0.25]);
}
steps = 360 / 30;
for ( i = [0 : 360 / steps : 360])
	rotate([0,0,i])
		rod();