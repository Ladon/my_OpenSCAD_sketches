$fn = 100; // raising detail

h = 1; // height
w = 1; // width
l = 100; // length

ring(100);

module ring(d = l) {
	linear_extrude(height = h) {
		difference() {
			circle(d = d);      //   larger disc
			circle(d = d- w); // - smaller disc
		}
	}
}

