$fs=0.1;
$fa=1;
// pin sizes //
pin_length = 34;
pin_diameter = 0.4;
pin_head_diameter = 1.35;
pin_head_length = 0.5;
//           //
extra_hole_space = 0.6;
extra_hole_space_tight = 0.2;
extra_pin_length = 10;
holder_diameter = 1.5;
tube_perimeter = holder_diameter - pin_diameter - extra_hole_space;
tube_length = 5;
holder_length = pin_length + extra_pin_length;
tube_length_tight = 0.5;
//           //
module pin () {
	cylinder(d = pin_diameter, h = pin_length);
	cylinder(d = pin_head_diameter, h = pin_head_length);
}

translate([0,0,- pin_length / 2 + tube_length / 2])
	%pin();
difference() {
	union () {
		cylinder(d = pin_diameter + extra_hole_space + tube_perimeter, h = tube_length);
		translate([0,holder_length / 2,tube_length / 2]) rotate([90,0,0])
			cylinder(d = holder_diameter, h = holder_length);
	}
	cylinder(d = pin_diameter + extra_hole_space, h = tube_length);
}
/*
difference() {
	translate([0,0,- tube_length_tight / 2 + tube_length / 2])
	cylinder(d = pin_diameter + extra_hole_space_tight + tube_perimeter, h = tube_length_tight);
	cylinder(d = pin_diameter + extra_hole_space_tight, h = tube_length);
}
*/
translate([0,0,tube_length / 2]) {
	rotate_extrude() {
		translate([(
			pin_diameter + extra_hole_space_tight) / 2,
			0,
			0
		])
		polygon([
			[0,tube_length_tight / 2],
			[(extra_hole_space - extra_hole_space_tight) / 2,
			tube_length / 2],
			[(extra_hole_space - extra_hole_space_tight) / 2,
			- tube_length / 2],
			[0,- tube_length_tight / 2]
		]);
	}
}

coil_length = 10;
translate([holder_length / 2,0,tube_length / 2]) {
	rotate([0,90,0])
		cylinder(h = coil_length);
}