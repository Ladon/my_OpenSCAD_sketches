rim_w = 20;
hole_d = 4;
hanger_w = hole_d;
hanger_h = 2 * hole_d;
hanger_space = hanger_h;
hangers_n = 3;
thickness = 2;
// -
main_part();
translate([0,-rim_w])
detachable_part();
// -
module main_part (w = rim_w, 
				   h = hangers_n 
					   * (hanger_space + hanger_h)
					   + hanger_space / 2
					   + rim_w + thickness, 
				   t = thickness, 
				   d = hole_d,
				   hanger_w = hanger_w,
				   hanger_space = hanger_space,
				   hanger_h = hanger_h)
{
	module inner(h = h) 
	{
		// Removal from bottom.
		// -
		cube([w, w, w]);
		// Removal from top.
		// -
		translate([0, t, w + t])
		cube([w - t, w, h - w - t]);
		// Removal of diagonal part.
		// -
		cot_top = (h - (w+t)) / (w);
		translate([0, t, h])
		rotate([-atan(cot_top),0])
		cube([w, h, 2 * w]);
		// Removal for hangers.
		// -
		for (hh = [(w + t) + hanger_h
				   : hanger_h + hanger_space: 
				   h - hanger_h])
		translate([w/2, t/2, hh])
		cube([hanger_w, 3 * t, hanger_h], center=true);
		// Screw hole.
		// -
		translate([w/2, w, w/2])
		rotate([90,0])
		cylinder(d = d, h = 6 * t, center=true, $fs=0.5);
	}
	difference() {
		cube([w, w + t, h]);
		inner();
	}
	%inner();
}

holder_l = 50;
module detachable_part(l = holder_l,
				   hanger_h = hanger_h, 
				   hanger_space = hanger_space,
				   w = hanger_w,
				   t = thickness)
{
	h = 2 * hanger_h;
	a = -7;
	module first_part ()
	{
		// Create holder.
		// -
		translate([0, 0, h / 2])
		cube([w, w + t, h]);
	}
	first_part();
	
	module second_part ()
	{
		// Recenter left side after rotation.
		// -
		recenter_shift = abs(sin(a) * h);
		// Horizontal shift. New length after rotation
		// is bigger than actual.
		// -
		rot_shift = l - abs(l * cos(a));
		module intersect_with() {
			translate([0, -recenter_shift])
			rotate([a,0])
			cube([w, l, h]);
		}
		// Corner's vertical shift.
		// -
		corner_drop = abs(l*sin(a));
		// Height of top right corner.
		// -
		new_h = h - corner_drop;
		// Horizontal length from top right
		// to bottom left in the rightmost small
		// hollow triangle.
		// -
		hor_crop = abs(new_h * tan(a));
		total_hor_crop = hor_crop + rot_shift;
		new_l = l - total_hor_crop;
		module intersected_with() {
			cube([w, l -total_hor_crop, h]);
		}
		mirror([0,1]) {
			// Form inclination.
			// -
			intersection() {
				intersect_with();
				intersected_with();
			}
			%intersect_with();
			%intersected_with();
		}
		module third_part ()
		{
			r = 2 * new_h;
			mirror([0, 1])
			{
				intersection()
				{
					translate([0,l - total_hor_crop, r])
					rotate([0,90])
					difference()
					{
						#cylinder(r = r, h = w, $fs = 0.5);
						cylinder(r = r/2, h = w, 
								 $fs = 0.5);
					}
					translate([0, l - total_hor_crop])
					cube([w,r,r]);
				}
			}
			module fourth_part ()
			{
				mirror([0,1])
				translate([0,new_l + r/2,r])
				cube([w,r/2,r/2]);
			}
			fourth_part();
		}
		third_part();
	}
	second_part();
}