translate([0,0,50])
    cube([10,10,100],center=true);
tri_points = [
    [0,0],
    [20,0],
    [0,40]];
module wing(angle=0) {
rotate(angle)
    translate([5,0,0])
        rotate([90,0,0])
            linear_extrude(height=2,center=true)
                polygon(tri_points);
}
wing();
wing(90);
wing(2*90);
wing(3*90);