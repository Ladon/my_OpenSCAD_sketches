// i think the AS6200 is 0.5mm tall
epoxy_thick     = 1.5;   // mm
as6200_thick    = 0.5;   // mm
safe_distance   = 0.5;   // mm
hole_diameter   = 2.6;   // mm
hole_radius     = hole_diameter/2;
// 21mm - 2.6=18.4
hole_x_dist     = 18.4;  // mm
// 14 - 2.6 = 11.4
hole_y_dist     = 11.4;  // mm
//
ceiling_thick   = 1;     // mm
for(i = [0,hole_x_dist])
for(j = [0,hole_y_dist])
    translate([i,j,0])
    color("yellow")
    cylinder(d=hole_diameter,h=epoxy_thick+as6200_thick,$fn=50);

difference() {
//
translate([-hole_radius,-hole_radius,epoxy_thick])
color("blue")
cube(
    [hole_x_dist+hole_diameter
    ,hole_y_dist+hole_diameter
    ,as6200_thick+safe_distance+ceiling_thick]);
//
translate([hole_radius,hole_radius,epoxy_thick])
color("red")
cube(
    [hole_x_dist-hole_diameter
    ,hole_y_dist-hole_diameter
    ,as6200_thick+safe_distance]);
}