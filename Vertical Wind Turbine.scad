$fs=0.1;
$fa=1;
for (i = [0:120:360])
	rotate([0,0,i])
		linear_extrude(height = 100, twist = 120)
			translate([30,0,0])
				wing();
module wing() {
	scale([1,0.2,1])
		circle(r=10);
}
	