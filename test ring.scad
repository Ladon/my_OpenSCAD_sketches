linear_extrude(height=40) {
    difference() {
        circle(r=35,$fn=50);
        circle(r=32,$fn=50);
    }
}