// scale([1.027,1.02,1])
translate([0,0,0.5]) {
    cube([100,2,1],center=true);
    cube([2,100,1],center=true);
    translate([0,50-0.5,0])
        cube([10,1,1],center=true);
    translate([50-0.5,0,0])
        cube([1,10,1],center=true);
    translate([0,-50+0.5,0])
        cube([10,1,1],center=true);
    translate([-50+0.5,0,0])
        cube([1,10,1],center=true);
}
