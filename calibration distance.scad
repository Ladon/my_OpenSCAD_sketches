r=95;
for(i=[-r:10:r],j=[-r:10:r]) {
    if(pow(i,2)+pow(j,2)<=pow(r,2)) {
        translate([i,j,0.5])
        color("blue")
        difference() {
            union() {
                cube([1,4,1],center=true);
                cube([4,1,1],center=true);
            }
            cube([0.5,0.5,1],center=true);
        }
    }
}