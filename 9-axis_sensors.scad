/* ToDo :
smaller balls
balls above epoxy!!
large text (whole rectangle)
*/
$fs = 0.1;
epoxy_width = 15.45;
epoxy_length = 25.8;
epoxy_height = 1.7;
hole_diameter = 3;
hole_radius = hole_diameter / 2;
elements_height = 1.7;
extra_space = 0.6;
perimeter_width = 0.7;
// hole distances measured from left and top ::
hole_pos = [[ 1.3, epoxy_width - 1], [ 21.6, epoxy_width - 1]];
///////////////////////////////
inner_cube_length = epoxy_length + extra_space;
inner_cube_width = epoxy_width + extra_space;
inner_cube_height = epoxy_height + elements_height + extra_space / 2;

outer_cube_length = inner_cube_length + 2 * perimeter_width;
outer_cube_width = inner_cube_width + 2 * perimeter_width;
outer_cube_height = inner_cube_height + perimeter_width;

translate([- extra_space / 2, - extra_space / 2]) {
	difference() {
		translate([ -perimeter_width, -perimeter_width,0]) color("yellow")
			cube([outer_cube_length, outer_cube_width, outer_cube_height]);
		color("navy") cube([inner_cube_length, inner_cube_width, inner_cube_height]);
		label();
	}
	%cube([inner_cube_length, inner_cube_width, inner_cube_height]);
}

module legs() {
	leg_height = elements_height + epoxy_height + extra_space / 2;
	for (i = hole_pos)
		translate(i + [hole_radius, - hole_radius]) {
			cylinder(d = hole_diameter, h = leg_height);
			// to find the section of a sphere and a cylinder, one can use 
			// the pythagorean theorem : ...
			protrusion = 0.125;
			sphere_radius = hole_radius + protrusion;
			sphere_diameter = 2 * sphere_radius;
			down_by = sqrt( pow(sphere_radius, 2) - pow( sphere_radius - protrusion, 2));
			difference() {
				translate([0,0,-down_by])
					sphere(r = sphere_radius);
				// remove the bottom part of the sphere
				erase_height = sphere_radius - down_by;
				translate([- sphere_radius, - sphere_radius,- 2 * down_by - erase_height])
					cube([ sphere_diameter, sphere_diameter, erase_height]);
			}
		}
}
legs();

module label() {
	label_height = 0.2;
	translate([0,0,outer_cube_height - label_height]) {
		linear_extrude(height = 0.5) {
			translate([0,8,0]) {
				text("    ↑", font = "DejaVu Sans:style=Bold", size = 8);
			}
			translate([0,2,0]) {
				text("       ⨀", font = "DejaVu Sans:style=Bold",  size = 5);
			}
			text("  ←", font = "DejaVu Sans:style=Bold",  size = 8);
		}
	}
}

