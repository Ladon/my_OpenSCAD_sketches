d = 100;
h = 2;
w = 10;
echo(w);
foot();
translate([0,0,h/2])
base();

module foot(w = w/4)
{
	l=d/2-3*w/2;
	phi = 1.618;
	h = d/phi;
	color("yellow")
	for (a = [0:360/4:359])
	{
		step=0.1;
		rotate([0,0,a])
		translate([-l,-l])
		for (p = [0:step:1-step])
		{
			a = p * 360/4;
			pre_x=w/2+p*l;
			post_x=w/2+(p+step)*l;
			CubePoints = [
			  [  -pre_x,  -pre_x,  p*h ],  //0
			  [ pre_x,  -pre_x,  p*h ],  //1
			  [ pre_x,  pre_x,  p*h ],  //2
			  [  -pre_x,  pre_x,  p*h ],  //3
			  [  -post_x,  -post_x,  (p+step)*h ],  //4
			  [ post_x,  -post_x,  (p+step)*h ],  //5
			  [ post_x,  post_x,  (p+step)*h ],  //6
			  [  -post_x,  post_x,  (p+step)*h ]]; //7
			  
			CubeFaces = [
		//	  [0,1,2,3],  // bottom
			  [4,5,1,0],  // front
		//	  [7,6,5,4],  // top
			  [5,6,2,1],  // right
			  [6,7,3,2],  // back
			  [7,4,0,3]]; // left

//			rotate([0,0,a])
			polyhedron( CubePoints, CubeFaces);
		}
	}
}
module base (d = d, h = h, w = w)
{
	color("olive")
	difference ()
	{
		cube([d, d, h], center=true);
		union ()
		{
			cube([d - w, d - w, h + 2], center = true);
			step = 10;
			hole_l = 1;
			hole_w = 0.4 * w;
			for (a = [0:360/4:359])
			{
				rotate([0,0,a])
				translate([-d/2, -d/2,0])
				{
				// Corner hole.
				// ------------
				cube([2*hole_l,2*hole_l,h + 2],center=true);
				for (i = [step:step:d-step])
				{
					translate([i,0,0])
					cube([hole_l, hole_w,h + 2], center = true);
				}
				}
			}
		}
	}
}
