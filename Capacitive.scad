/*
 * Diameter : 17.75 mm
 * Height   : 68.55 mm
 * bottom   : 9 mm
 * paximadi : 23.8 mm
 * rodela   : 30.55 mm
 * 1.1 from bottom  : 19.6
 * 1.2 ...
 * 2.1 from bottom  : 1.7
 * 2.2 ...
 * 1.1 from left    : 5
 * 1.2 from left    : 21.9
 * 2.1 ...
 * 2.2 ...
 * s(crew)d(iameter)    : 2.9 (3mm)
 * h(hole)d             : 2.4 
 * allen d  : 2.8
 * allen hd : 2.4
 */
$fn=100;
convexity=100;
hd=2.4;
sd=2.9;
hr=hd / 2;
h=10;
//translate([d/2,d/2]) cylinder(d=d,h=h);
hole=[[5,19.6],[21.9,19.6],[5,1.7],[21.9,1.7]];
alen=[[13.5,1.7],[13.5,9.9]];
asd=2.8;
asr=asd/2;
ahd=2.4;
ahr=ahd/2;
hmx = (hole[2][0]+hole[3][0])/2;
hmy = (hole[2][1]+hole[0][1])/2;
cube_l = hole[1][0] - hole[0][0] + hd + 2 * hole[0][0];
cube_w = hole[0][1] - hole[2][1] + hd + 2 * hole[2][1];
cube_h = 2;
scale([1.027,1.02,1])
translate([0,cube_w,0])
color("indigo")
    cube([cube_l,3,cube_h]);
scale([1.027,1.02,1])
difference () {
    translate([hmx + hr,hmy + hr,cube_h / 2]) 
        color("navy")
        cube([cube_l,cube_w,cube_h],center=true);
    color ("yellow") {
        for (i = [0:3]) {
        //    x = hole[i][0];
        //    y = hole[i][1];
            translate(hole[i]+[hr,hr]) 
                cylinder(d=sd,h=h);
        }
        translate(alen[0] + [ahr,ahr])
            cylinder(d=asd,h=h);
        translate(alen[1] + [ahr,ahr])
            cylinder(d=asd,h=h);
    }
}
holder_w=3.6;
holder_l=cube_l;
holder_h=28;
holder_x=0;
holder_y=alen[1][1] + 1.2*asd;
holder_z=cube_h;

nerve_w=2;
nerve_l=holder_y-10;
module nerve (x=0,y=0,z=0,l=nerve_l,w=nerve_w,h=6) {
    translate ([holder_z + x,0 + y + 10,holder_z + z])
    rotate([0,-90,0])
    linear_extrude(height=w)
        polygon([[0,0],[0,l],[h,l]]);
}

module nerves_1 () {
    nerve (h=11);
    nerve (1*nerve_w,h=8);
    nerve (2*nerve_w);
    nerve (3*nerve_w,h=4.8);
    nerve (4*nerve_w,h=4);
    nerve (cube_l-nerve_w-4*nerve_w,h=4);
    nerve (cube_l-nerve_w-3*nerve_w,h=4.8);
    nerve (cube_l-nerve_w-2*nerve_w);
    nerve (cube_l-nerve_w-1*nerve_w,h=8);
    nerve (cube_l-nerve_w,h=11);
}
//nerves_1();
//rotate([0,0,180])
//nerves_1();

cap_d=17.75;
cap_r=cap_d/2;
cap_h=holder_w*2;
cap_x=cube_l/2;
cap_y=holder_y-holder_w/2;
cap_z=cap_d/2 + 12.55;
scale([1.027,1.02,1])
difference () {
    translate([holder_x,holder_y,holder_z])
        color("Maroon")
        cube([holder_l,holder_w,holder_h]);
    {
        translate([cap_x-cap_r,cap_y,cap_z])
        cube([cap_d,cap_h,2*cap_d]);
        translate([cap_x,cap_y,cap_z])
        rotate([-90])
        color("olive")
        cylinder(d=cap_d,h=cap_h);
    }
}

rod_d = 30.6;
scale([1.027,1.02,1])
difference() {
    {
        nerves_1();
    }
    translate([cap_x,cap_y,cap_z])
    rotate([-90,0,0])
    color("olive")
        cylinder(d=rod_d,h=cap_h);
}