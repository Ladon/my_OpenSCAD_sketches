//$fs=1; // more detail
//$fa=1; // more detail
// everything is in millimeters
wing_length = 20;
wing_width = 0.1; // 20% of wing_length
turbine_radius = 50;// space between central rod and wings
wing_height = 150;
wings = 3;

wing_holder_height = wing_height / 5;
module wing_holder(vertical_height = wing_holder_height) { // a diagonal "rod"
	width = wing_width * wing_length * 5;
	height = 0.4; // 20% of width
	A = vertical_height; // vertical height of rod
	B = turbine_radius; // horizontal length of rod
	length = sqrt( pow(A,2) + pow(B,2)); // hypotenuse
	translate([B,0,0])
	rotate([0,-(90 - atan(A/B)),0]) // arctangent
	scale([height,1,1])
		cylinder(d = width, h = length);
}

wing_holder_pos = wing_height / 6;
wing_holder_pos_high = 5 * wing_holder_pos;
for (i = [0:360 / wings:360])
	rotate([0,0,i]) {
		color("cyan")
		linear_extrude(height = wing_height, twist = 360 / wings)
		translate([turbine_radius,-wing_length/2,0])
		rotate([0,0,-90])
			airfoil(naca=wing_length * wing_width * 10,L = -wing_length);
		translate([0,0,wing_holder_pos])
		rotate([0,0,-(wing_holder_pos) / wing_height * 360 / wings])
		color("Khaki")
			wing_holder();
		translate([0,0,wing_holder_pos_high])
		rotate([0,0,(wing_holder_pos) / wing_height * 360 / wings])
		color("Khaki")
		rotate([180,0,0])
			wing_holder();
	}

// main cylinder //
main_cylinder_extra_bottom = 70;
main_cylinder_d = wing_width*wing_length * 5;
translate([0,0,-main_cylinder_extra_bottom])
color("lightgreen")
	cylinder(d = main_cylinder_d,h = wing_holder_pos_high - wing_holder_height + main_cylinder_extra_bottom);

// stop cone //
stop_cone_w = 4;
stop_cone_h = 10;
stop_cone_pos = -15;

triangle_points = [[0,0],[0,stop_cone_h],[stop_cone_w,0]];
translate([0,0,stop_cone_pos])
color("OrangeRed")
rotate_extrude()
translate([main_cylinder_d / 2 - 0.01,0])
polygon(triangle_points);



// the following code to the end is copied from HooptieJ 
// @ https://www.thingiverse.com/thing:1250755/#files 
// and is licensed under the Creative Commons - Attribution - 
// - Non-Commercial license

module airfoil(naca=12, L =100, N = 81, open = false)
{
  polygon(points = airfoil_data(naca, L, N, open)); 
}
//

// this is the main function providing the airfoil data
function airfoil_data(naca=12, L = 100, N = 81, open = false) = 
  let(Na = len(naca)!=3?NACA(naca):naca)
  let(A = [.2969, -0.126, -.3516, .2843, open?-0.1015:-0.1036])
  [for (b=[-180:360/(N):179.99]) 
    let (x = (1-cos(b))/2)  
    let(yt = sign(b)*Na[2]/.2*(A*[sqrt(x), x, x*x, x*x*x, x*x*x*x])) 
    Na[0]==0?L*[x, yt]:L*camber(x, yt, Na[0], Na[1], sign(b))]; 

// helper functions
function NACA(naca) = 
  let (M = floor(naca/1000))
  let (P = floor((naca-M*1000)/100))
  [M/10, P/10, floor(naca-M*1000-P*100)/100];

function camber(x, y, M, P, upper) = 
  let(yc = (x<P)?M/P/P*(2*P*x-x*x): M/(1-P)/(1-P)*(1 - 2*P + 2*P*x -x*x))
  let(dy = (x<P)?2*M/P/P*(P-x):2*M/(1-P)/(1-P)*(P-x))
  let(th = atan(dy))
  [upper ? x - y*sin(th):x + y*sin(th), upper ? yc + y*cos(th):yc - y*cos(th)];