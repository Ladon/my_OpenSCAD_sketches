// detail ::
$fn=50;

l = 50;
h = 3;
w = 15;
hole_h=2;
hole_l=2;
axis_d=3;
axis_r=axis_d/2;
handle_l = 7;
handle_h=h; // alias
handle_w=1.3;
angle=30;
s=2;
upscale=s;
downscale=1-(s-1);

n_o_wings=3;
module wings_in_place(s=s)
{
	module wing_w_handle(s=s)
	{
		module handle(l=handle_l,h=h,w=handle_w)
		{
			translate([0,-l/2,h/2])
			difference() {
				cube([w,l,h],center=true);
				translate([0,0,hole_h/2])
				cube([w,hole_l,hole_h],center=true);
				translate([0,-l/2,0])
				scale(s)
				cube([axis_d,axis_d,h],center=true);
				echo(s);
			}
			// starts at z-axis
		}
		module wings(l=100,w=10,h=2)
		{
			wing_pts = [[-3,0],[0,1],[10,0]];
			def = wing_pts[2][0] - wing_pts[0][0];

			translate([0,l/2,0]) // TIDY
			scale([w,l,h])
			scale([1/def,1,1])
			translate([0,1/2,0])
			rotate([90,0,0])
			linear_extrude(height=1)
			polygon(points=wing_pts);
			// starts at z-axis
		}

		scale(s)
		wings(l,w,h);
		handle();
	}

	for (i = [0:360/n_o_wings:360-0.1])
		rotate([0,angle,i])
		translate([0,handle_l,0])
		color("yellow")
		wing_w_handle(s);
}

ring_w=1.5;
ring_h = handle_h*cos(angle)+ring_w;
ring_r=handle_l;
ring_d=2*ring_r;

//translate([0,0,50+510])
//ring_bottom(ring_h);

//translate([0,0,50+20]) // 1/3 
//color("yellow")
//wings_in_place();

module ring_bottom(ring_h = ring_h)
{
	ring_z = -ring_w-s*(handle_w/2)*sin(angle); // place it ring_w below wings
	difference() {
		translate([0,0,ring_z])
		color("navy")
		cylinder(r=handle_l,h=ring_h);
		translate([0,0,ring_w+ring_z])
		cylinder(r=handle_l-ring_w,h=ring_h-ring_w);
		axis(s); // upscaled
		wings_in_place(s); // upscaled
	}
}

axis_h = ring_h+15;
module axis(s=1)
{
	color("olive")
	translate([0,0,-axis_h/2+ring_h])
	scale(s)
	cylinder(d=axis_d,h=axis_h,center=true);
}
module ring_top(s=s)
{
	difference() {
		translate([0,0,s*(handle_w/2)*sin(angle)])
		color("magenta")
		cylinder(d=(ring_d-2*ring_w*s),h=ring_h);
		wings_in_place(s);
	}
}
//translate([0,0,50+30]) {
//	ring_top();
//	axis();
//}


//ring_top();
axis();
wings_in_place();
//ring_bottom();