/*
 * Turbine Airfoil
 *
 * * * * * * * * * */

// the following code is adapted from HooptieJ 
// @ https://www.thingiverse.com/thing:1250755/#files 
// and is licensed under the Creative Commons - Attribution - 
// - Non-Commercial license

module airfoil (length = 70, thickness = 3, detail = 27, open = false)
{
	// thickness is currently approximate
	// need to see what is naca or how it relates to thickness
	translate([-0.3*length,0])
	polygon(points = airfoil_data(thickness * 10 * 4 / 6, length, detail, open)); 
}
//

// this is the main function providing the airfoil data
function airfoil_data(naca=12, L = 100, N = 81, open = false) = 
  let(Na = len(naca)!=3?NACA(naca):naca)
  let(A = [.2969, -0.126, -.3516, .2843, open?-0.1015:-0.1036])
  [for (b=[-180:360/(N):179.99]) 
    let (x = (1-cos(b))/2)  
    let(yt = sign(b)*Na[2]/.2*(A*[sqrt(x), x, x*x, x*x*x, x*x*x*x])) 
    Na[0]==0?L*[x, yt]:L*camber(x, yt, Na[0], Na[1], sign(b))]; 

// helper functions
function NACA(naca) = 
  let (M = floor(naca/1000))
  let (P = floor((naca-M*1000)/100))
  [M/10, P/10, floor(naca-M*1000-P*100)/100];

function camber(x, y, M, P, upper) = 
  let(yc = (x<P)?M/P/P*(2*P*x-x*x): M/(1-P)/(1-P)*(1 - 2*P + 2*P*x -x*x))
  let(dy = (x<P)?2*M/P/P*(P-x):2*M/(1-P)/(1-P)*(P-x))
  let(th = atan(dy))
  [upper ? x - y*sin(th):x + y*sin(th), upper ? yc + y*cos(th):yc - y*cos(th)];