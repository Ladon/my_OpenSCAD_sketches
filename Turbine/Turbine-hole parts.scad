include <Turbine-variables.scad>

module hole_parts(R = radius, L = blade_length, N = wings, CCW = true, W = width) {
	new_d = 4 * W / 3; // depth
//		d = 2 * W / 3; // depth
	new_R = L/2;
//		translate([0,0,H - d])
	step = 360 / N; // angle to next blade
	for (i = [0: step: 360]) {
		translate([new_R * cos(i), new_R * sin(i), new_d / 2]) // x, y coordinates
//			translate([R * cos(i), R * sin(i), d / 2]) // x, y coordinates
			rotate([0, 0, i + (CCW ? -90 : 90)])
//					cube([L / 3, W / 3, new_d] , center = true);
				cube([L / 3-0.15, W / 3-0.15, new_d] , center = true);
	}
}
color("red") hole_parts();
