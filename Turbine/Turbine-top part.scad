/*
 * Turbine Top Part
 *
 * * * * * * * * * */
include <Turbine-variables.scad>
use <Turbine-blades.scad>

/*
importance : 
height
radius
width
number
cw vs ccw
*/
ccw = false;
top = true;
//                    //
// strengthening ring //
//                    //
module ring(R = radius, N = wings, CCW = ccw, W = width) {
	echo("width = ",width);
	ring_h = 2;
	loose = 1;
	hole_d = axis_d + loose;
	ring_w = W*1.2;
	
	/// outer ring ///
	linear_extrude(height = ring_h)
	difference() {
		circle(r = R + ring_w / 2);
		circle(r = R - ring_w / 2);
	}

	/// axis ///
	angle = asin((hole_d/2+ring_w/2)/R);
	cube_l = cos(angle)*R;
	
	for (i = [0:360/N:360])
		rotate([0,0,i])
		translate([radius,0,0])//attach to "blade"//
		rotate([0,0,180])//"flip" over y//
		rotate([0,0,angle * (CCW ? -1 : 1)])
		translate([0,-ring_w/2,0])//center y//
		color("yellow")
		cube([cube_l,ring_w,ring_h]);

	/// inner ring ///
	sphere_d=hole_d+2*ring_w-3;
	echo(sphere_d);
	inner_ring_h = ring_h + sphere_d/2;
	difference(){
		linear_extrude(height = inner_ring_h)
			circle(d = hole_d + 2*ring_w);
		translate([0,0,ring_h+sphere_d/2])
		sphere(d=sphere_d);
	}
}

/// change CCW for the top part ///
module top_part(H = height_half, R = radius, L = blade_length, N = wings, CCW = ccw, W = width, TOP = top) {
	ring(R, N, CCW, W);
	blades(H, R, L, N, CCW, W, TOP);
}
top_part();