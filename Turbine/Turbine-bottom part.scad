/*
 * Turbine Bottom Part
 *
 * * * * * * * * * */
include <Turbine-variables.scad>
use <Turbine-blades.scad>

/*
importance : 
height
radius
width
number
cw vs ccw
*/
ccw = true;
top = false;
//                    //
// strengthening ring //
//                    //
module ring(R = radius, N = wings, CCW = ccw, W = width) {
	ring_h = 2;
	loose = 3;
	hole_d = axis_d + loose;
	echo(hole_d);
	ring_w = W*1.2;
	
	/// outer ring ///
	linear_extrude(height = ring_h)
	difference() {
		circle(r = R + ring_w / 2);
		circle(r = R - ring_w / 2);
	}

	/// axis ///
	angle = asin((hole_d/2+ring_w/2)/R);
	cube_l = cos(angle)*R;
	
	for (i = [0:360/N:360])
		rotate([0,0,i])
		translate([radius,0,0])//attach to "blade"//
		rotate([0,0,180])//"flip" over y//
		rotate([0,0,angle * (CCW ? -1 : 1)])
		translate([0,-ring_w/2,0])//center y//
		color("yellow")
		cube([cube_l,ring_w,ring_h]);

	/// inner ring ///
	linear_extrude(height = ring_h)
	difference() {
		circle(d = hole_d + 2*ring_w);
		circle(d = hole_d);
	}
}

module bottom_part() {
	ring(radius, wings, ccw, width);
	blades(height_half, radius, blade_length, wings, ccw, width, top);
}
bottom_part();