/*
 * Turbine Blades
 *
 * * * * * * * * * */
include <Turbine-variables.scad>
include <Turbine-airfoil.scad>

module blades(H = height_half, R = radius, L = blade_length, N = wings, CCW = true, W = width, TOP = true)
{
	//        //
	// blades //
	//        //
	step = 360 / N; // angle to next blade
	module wings() {
		linear_extrude(height = H, twist = step / 2) // height
		for (i = [0: step: 360]) {
			translate([R * cos(i), R * sin(i)]) // x, y coordinates
				rotate([0, 0, i + (CCW ? -90 : 90)])
					airfoil(L,W);
		}
	}
	
	if (TOP == true)
		union() {
			color("navy") wings();
			intersection () {
				holes();
				translate([0,0,height_half])
				rotate([0,0,180/N])
				wings();
			}
		}
	else
		difference() {
			color("olive") wings();
			holes(height_half, radius, blade_length, wings, true, width, false);
		}
}
blades(height_half, radius, blade_length, wings, false, width, true);


//       //
// holes //
//       //
module holes(H = height_half, R = radius, L = blade_length, N = wings, CCW = true, W = width, TOP = true) {
	loose = 1;
	hole_w = (TOP? W/2 : 3*W/4);
	hole_l = L/5 + (TOP? 0: loose);
	clip_d = L/5 + (TOP? 0: loose/2);
	step_h = 1;
	step = 360 / N; // angle to next blade
	d = 2 * W / 3; // depth
	translate([0,0,H - d])
	for (i = [0 + 180: step: 360 + 180]) {
//		translate([R * cos(i), R * sin(i), d / 2]) // x, y coordinates
		translate([R * cos(i), R * sin(i), d]) // x, y coordinates
		rotate([0, 0, i + (CCW ? -90 : 90)])
		translate([0,-hole_w/2+(TOP?0:loose/2),0])
		rotate([0,(TOP ? 0 : 180),0])
		translate([0,0,step_h/2-0.01])
		color((TOP?"navy":"olive"))
		union() {
			translate([0,0,step_h/2])
			linear_extrude(height = clip_d,scale=[1.5,1])
			square([hole_l, hole_w],center=true);
			cube([hole_l, hole_w, step_h] , center = true);
		}
//				cube([L / 3, W / 3, d] , center = true);
	}
}