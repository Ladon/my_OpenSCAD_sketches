/*
 * Turbine Combination
 *
 * * * * * * * * * */
include <Turbine-variables.scad>
use <Turbine-top part.scad>
use <Turbine-bottom part.scad>

translate([0,0,2*height_half])
rotate([0,180,180/wings])
top_part();
bottom_part();