
module my_text(str,val="top")
{
	linear_extrude(height = 10)
	text(str,font="Meslo LG L:style=Regular",size=18,valign=val,halign="center");
}
module my_cube(side = 100)
{
	translate([0,0,side/2]) {
		cube(side,center=true);
		translate([0,-side / 2 - side / 10,-side/2])
		linear_extrude(height = 10)
		text("cube 1cm",font="Meslo LG L:style=Regular",size=18,valign="top",halign="center");
	}
}
module my_rings(diameter = 100)
{
	module ring(diameter = diameter, width = 10)
	{
		difference() {
			circle(d = diameter);
			circle(d = diameter - width);
		}
	}
	for (diameter = [10 : 20 : diameter])
		linear_extrude(height = 10)
		ring(diameter);
	translate([0,-diameter / 2 - 10])
	my_text("10mm spacing");
}

module my_rectangle(side = 200)
{
	w = 10;
	h = 10;
	difference() {
		cube([side + 2 * w,side + 2 * w,h]);
		translate([w, w,-1])
		cube([side, side,12]);
	}
	translate([w+side/2,side/2])
	my_text("20x20cm inner","bottom");
}

module my_modules()
{
	translate([-200,0])
	my_cube();
	my_rings();
	translate([100,0])
	my_rectangle();
}
my_modules();