//$fs = 1; // fragment size in mm (wiki)
//$fa = 5; // Fragment angle in degrees (wiki);
$fn = 60; // increase detail
gl = 108.75; // epoxy length
gw = 53.35; // epoxy width
sh = 8.6;	    // height above epoxy
eh = 1.6;   // epoxy height
ph = 1.75;   // pin height
gh = sh + eh + ph; //
feet_h = gh + 2;  // pillars will reach 2mm below pins
feet_d = 3;
difference() {
	color("yellow") union() {
		cube([gl, gw, sh]); // the outer box
	}
	{
		perimeter_w = 5; // at around 5.7 from top is a component near the capacitor
		inner_l = gl - 2 * perimeter_w;
		inner_w = gw - 2 * perimeter_w;
		inner_h = max(sh - perimeter_w,sh - 2); // 2nd is arbitrary
		translate([perimeter_w, perimeter_w, 0])
			cube([inner_l, inner_w, inner_h]);
		color("magenta") {
			female_pins();
			audio();
			buttons();
			power();
			usb();
			SD();
			WiFi();
			mic();
			capacitor();
			serial_male_pins();
			cspi();
		}
	}
}
feet();

// modules //
module cspi() {
	w = 6.3;
	l = 4.95;
	h = 8.65;
	y = 23.95;
	x = 62;
	translate([x, y, 0])
		cube([l, w, h]);
}
// serial male pins //
module serial_male_pins () {
	y = 25.65;
	x = 12;
	l = 2.5; // length
	w = 10; // width
	h = 10.4 - eh; // height
	translate([x, y, 0])
		cube([l, w, h]);
}
// capacitor //
module capacitor () {
	d = 6.3; // diameter
	h = 9.65 - eh; // height above epoxy
	x = 19.3 - 0.35 - d; // approx
	y = 45.75 - d;
	translate([x, y, 0])
	translate([d / 2, d / 2, 0])
		cylinder(d = d, h = h);
}
// microphone //
module mic () {
	/* rotations are around the base's center
	 * differencies are transfered and thus negated
	 * so, remember to re-add the part that was negated
	 * */
	d = 4.1; // diameter
	h = 3.3 - eh; // height of mic
	a = 88; // angle
	translate([23.5 - d, 1.5, 0])
		translate([d / 2, d / 2, 0]) {
			difference() {
				// remove the bottom part due to 
				// rotation going under
				z = d / cos(a); // intersection
//				translate([0, 0, h])
//				translate([0, (z - d) / 2, h])
				echo (z , d, (z - d) / 2, (z - d) / 4);
//				translate([0, (z - d) / 2, 0])
				new_z = - d / 2 * sin(a); // make it glue at highest point of duct
				tan_a = sin(a) / cos(a);
				new_y = -new_z * tan_a; // while we put it lower, its y pos changes. This fixes it
				translate([0, 0, new_z + h]) // also put it above the mic (h)
					scale([1, cos(a), 1])
						translate([0, new_y, 0])
							rotate([a, 0, 0])
								cylinder(d = d, h = 200);
				translate([0, 0, h / 2])
					cube([2 * d, 2 * d, h], center = true);
			}
			cylinder(d = d, h = h);
		}
}
// antena WiFi //
module WiFi () {
	l = 10.2;
	w = 3;
	h = 3.6 - eh;
	translate([105.4 - l, 52.45 - w, 0])
		cube([l,w,h]);
}
// micro SD //
module SD () {
	l = 14.1;
	w = 14.8;
	h = 3.5 - eh;
	translate([0,23.25 - w,0])
		cube([l,w,h]);
}
// usb micro //
module usb() {
	l = 5;
	w = 7.7;
	h = 4.5 - eh;
	translate([0,33.8-w,0])
		cube([l,w,h]);
}
// power jack //
module power () {
	l = 10.85;
	w = 10.2;
	h = 9.3 - eh;
	translate([0,47.1 - w,0])
		cube([l,w,h]);
}
// 3x buttons //
module buttons () {
	lb = 7.9; // length big
	ls = 7;    // length short
	w = 2.85;
	hb = 5.2 - eh;
	hs = 2.6 - eh;
	translate ([10.35 - lb, 0, 0])
		button();
	translate ([102.45 - lb, 0, 0])
		button();
	translate([lb + (10.35 - lb),gw,0])
		rotate([0,0,180])
			button();
	// modules //
	module base () {
		cube([lb,w,hs]);
	}
	module upper () {
		cube([ls,w,hb]);
	}
	module button() {
		base ();
		translate ([(lb - ls) / 2 ,0 ,0])
			upper ();
	}
}
// audio //
module audio () {
	l = 12.15;
	w = 6.1;
	h = 6.95 - eh;
	translate([gl - l, 45.25 - w ,0])
		cube([l,w,h]);
	translate([gl - l, 35.1 - w ,0])
		cube([l,w,h]);
	translate([gl - l, 25 - w ,0])
		cube([l,w,h]);
	phh = 6.4 - eh;
	translate([gl - l, 14.3 - w ,0])
		cube([l,w, phh]);
}
// female pins //
module female_pins () {
	l_a = 47.5;
	w = 2.5;
	translate([65 - l_a, gw - 1.2 - w,0])
		cube([l_a,w,sh]);
	l_b = 23.5;
	translate([92.7 - l_b, gw - 1.2 - w, 0])
		cube([l_b,w,sh]);
	l_c = 38.4;
	translate([65 - l_c, 1.2,0])
		cube([l_c,w,sh]);
//	l = 23.5;
	translate([92.7 - l_b, 1.2, 0])
		cube([l_b,w,sh]);
}
// feet //
module feet() {
	feet_pos =
	[
		[12.45, 49.2],
		[12.45, 0.95],
		[64.4, 34],
		[64.4, 6],
		[91.65, 34],
		[91.65, 6]
	];
	for (i = feet_pos) {
		foot(i[0],i[1]);
	}
	// modules //
	module foot(x,y) {
		feet_r = feet_d / 2;
		translate([x + feet_r, y + feet_r, -(feet_h - sh)]) {
			cylinder(d =  feet_d,h = feet_h);
		}
	}
}
