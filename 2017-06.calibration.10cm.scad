$fn = 100; // raising detail

h = 2; // height
w = 2; // width
l = 100; // length

rod(l + 20);
rotate([0,0,90])
	rod(l + 20);
for (i = [1:2:5]) {
	ring(i * l / 5);
}

translate([(l + 20) / 2 - w / 2, 0, 0])
	rotate([0,0,90])
		rod(10);

translate([0, (l + 20) / 2 - w / 2, 0])
	rod(10);
	
module rod(l = l) { // length (l) x width (w) x height (h)
	translate([0,0,h/2]) { // raise
		cube([l,w,h],center=true);
	}
}

module ring(d = l) {
	linear_extrude(height = h) {
		difference() {
			circle(d = d);      //   larger disc
			circle(d = d- w); // - smaller disc
		}
	}
}

