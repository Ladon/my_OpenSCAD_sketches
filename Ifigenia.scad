$fn = 100;
pi = 3.141592;
c = 3e8;
f = 1e9;
// phase = 0; look at t
T = 1 / f;
l = c / f;
echo(l);
t = 0;
e_x = 40*l;
e = [[-e_x,0],[e_x,0]];
A = 2.5;
max_A = 2 * A;
detail = 2;
max_x = 50;
points = [for(i=[-max_x:detail:max_x], j=[-max_x:detail:max_x])
    [i,j]];
function r(x,y) =
    sqrt(pow(x[0] - y[0],2) + pow(x[1] - y[1],2));
function z(x) =
    2 * A * cos(2 * pi * (r(x,e[0]) - r(x,e[1])) / (2 * l)) 
    * sin(2 * pi * (t / T - (r(x,e[0]) + r(x,e[1])) / (2 * l)));
function a(x) =
    abs(2 * A * cos(2 * pi * (r(x,e[0]) - r(x,e[1])) / (2 * l)));
zero_h = 1;
for(i = points) {
	if(pow(i[0], 2) + pow(i[1], 2) <= pow(2 * e_x, 2))
	color([a(i) / max_A, 0, 1 - a(i) / max_A])
	translate(i)
	translate([0,0,a(i)/2 + zero_h / 2])
	cube([detail + 0.01,detail + 0.01,a(i) + zero_h],center = true);
}
color("navy") {
	for (i = [-1,1])
		translate([i * e_x,0,max_A / 2 + zero_h / 2])
			cylinder(d = detail, h =  max_A + zero_h, center = true);
}
