/* [Main parameters] */

// Number of wings
wings = 3;

// Single wing's diameter
d 	= 50;

// Width of wings and hole
w 	= 1.5;

// Height
h = 100;

// Hole's diameter
hole_d = 5;

/* [Extra parameters] */

// Hole stop's relative to height position
cap_h = 0.5;

// Detail specific: minimum size
$fs = 0.5;

// Detail specific: minimum angle
$fa = 2;

/* [Hidden] */

// Relative variables
// -
r	= d/2;
hole_r = hole_d / 2;

// Alternative to using both $fs and $fa
// -
//$fn = 3*50;	// *detail* - //

module base_wing()
{
	translate([-r,-hole_r])
	difference()
	{
		circle(d = d);
		circle(r = r - w);
		translate([0,r])
		square([d,d],center=true);
	}
}

module wing_turbine(wings = wings, h = h, cap_h = cap_h)
{
	#linear_extrude (height = h, twist = 360 / wings)
	{
		for (i = [0:360/wings:360-1])
		{
			rotate([0,0,i])
			base_wing();
		}
		base_axel();
	}
	// Put a cap at the top end of the hole.
	// -
//	translate([0,0,h])
//	axel_cap();

	// Just a bit above center mass.
	// -
//	translate([0,0,3/5*h])
	
	// Make it reversible by putting two reverse caps at
	// the center.
	// -
	translate([0,0,h * cap_h])
	double_cap();
}

module base_axel(r = hole_r, w = w)
{
	difference()
	{
		circle(r = r + w);
		circle(r=r);
	}
}

module axel_cap (r = hole_r, w = w, center = false)
{
	r = hole_r + w;
	center_z = -r + w/2;
	translate([0,0,center?center_z:0])
	difference()
	{
		sphere(r = r);
		sphere(r = r - w);
		translate([0,0,-r])
		cylinder(r = r, h = r);
	}
}

module double_cap (r = hole_r, w = w)
{
	axel_cap(r = r, w = w, center = true);
	rotate([180,0,0])
	axel_cap(r = r, w = w, center = true);
}

wing_turbine();
