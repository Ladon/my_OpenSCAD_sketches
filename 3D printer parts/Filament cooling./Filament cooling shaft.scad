// Wherever it is necessary:
//	length  --> X
//	width	--> Y
//	height	--> Z
// -------------------------
thickness = 1;
t = thickness;
// The place where the screws go.
// ------------------------------
back_length = 100;
back_width = back_length;
module back ()
{
	l = back_length;
	w = back_width;
	h = 2*t;
	screw_d = 0.03 * l;
	tmp = 0.3 * l;
	screw_positions = [
		[tmp,tmp],
		[-tmp, tmp],
		[-tmp, -tmp],
		[tmp, -tmp]
	];
	// Draw.
	// -----
	translate([0,0,h/2])
	difference ()
	{
		cube([l, w, h], center=true);
		union ()
		for (pos = screw_positions)
		{
			translate(pos)
			cube([screw_d, screw_d, h + 2], center=true);
		}
	}
}
shaft_width = back_length;
shaft_ratio = 0.5;
shaft_small_width = shaft_ratio * shaft_width;
shaft_length = 0.2 * shaft_width;
shaft_height = back_length;
module right_shaft ()
{
	l = shaft_length;
	w = shaft_width;
	small_w = shaft_small_width;
	h = shaft_height;
//	translate([w/2 + l/2,0,h/2])
//	difference ()
//	{
//		cube([l, w, h],center=true);
//		t = thickness;
//		cube([l-2*t, w-2*t, h + 2],center=true);
//	}
	translate([back_length/2,-w/2,h])
	mirror([0,0,1])
	difference()
	{
		linear_extrude(height=h,scale=[1,shaft_ratio])
		square([l,w]);
		scaling_compensation = (small_w - 2 * t) / w;
		c = scaling_compensation;
		translate([t,t,-1])
		linear_extrude(height=h+2,scale=[1,c])
		square([l-2*t,w-2*t]);
	}
}
module right_ear ()
{
	w = back_length;
	l = 0.2*w;
	h = l;
	translate([back_length/2 + l/2,0,shaft_height+h/2])
	difference()
	{
		cube([l,w,h],center=true);
		t = thickness;
		translate([-t,0,-t])
		cube([l,w-2*t,h],center=true);
	}
}

module shoulders ()
{
	l = 2 * shaft_length + back_length;
	w1 = shaft_small_width;
	w2 = 0.3 * back_width;
	w = w1 + w2;
	h = shaft_length;
	mirror([0,0,1])
	translate([-l/2,-w,0])
	difference()
	{
		linear_extrude(height=h,scale=[1,0])
		square([l,w]);
		difference()
		{
			translate([t,t,-t])
			linear_extrude(height=h,scale=[1,0])
			square([l-2*t,w-2*t]);
			// Anti-translate.
			// ---------------
			translate([+l/2,+w/2+w2/2,0])
			translate([-back_length/2,-w1/2])
			cube([back_length,w1,h+2]);
		}
		translate([+l/2+t,+w/2+w2/2+t,-1])
		translate([-back_length/2,-w1/2])
		cube([back_length-2*t,w1-t,h+2+1]);
	}
}
// Draw all.
// ---------
union ()
{
	back();
//	right_shaft();
	right_ear();
	mirror([1,0,0])
	{
		right_shaft();
		right_ear();
	}
	shoulders();
}
right_shaft();
