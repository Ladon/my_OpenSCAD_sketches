/*
 * expect errors!
 * all mesurements were taken by hand 
 * with vernier scale tools.
 * * */

$fs = 0.1; // minimum fragment size in mm (wiki)
$fa = 8; // minimum angle for a fragment (i suppose 3 ~ max 120 fragments/circle)
//$fn = 300; // ignore others. is parts per circle
/* sorry, my printer needs scaling, currently.
 */ 
//x_scale = 100/101.1;
//y_scale = 100/101.55;
x_scale = 100/101;
y_scale = 100/101;
//x_scale = 0.99;
//y_scale = 0.99;
x_scale = 1;
y_scale = 1;
/* */
gl = 108.75; // epoxy length
gw = 53.35; // epoxy width
sh = 8.6;	    // height above epoxy
eh = 1.6;   // epoxy height
ph = 1.75;   // pin height
gh = sh + eh + ph; //
empty_space = 0.3; // for all holes
perimeter_w = 5; // at around 5.7 from top is a component near the capacitor
ceiling_w = min(perimeter_w, 2); // 2nd is arbitrary
translate([0, 0, -eh]) 
	scale([x_scale,y_scale,1])
		%cube([gl, gw, eh]);	// epoxy
difference() {
	color("yellow") union() {
		scale([x_scale,y_scale,1])
			cube([gl, gw, sh]); // the outer box
	}
	{
		inner_l = gl - 2 * perimeter_w;
		inner_w = gw - 2 * perimeter_w;
		inner_h = sh - ceiling_w;
		translate([perimeter_w, perimeter_w, 0])
			scale([x_scale,y_scale,1])
				cube([inner_l, inner_w, inner_h]);
		scale([x_scale,y_scale,1])
			color("magenta") {
				female_pins();
				audio();
				buttons();
				power();
				usb();
				SD();
				WiFi();
				mic();
				capacitor();
				serial_male_pins();
				cspi();
			}
	}
}
difference() {
	scale([x_scale,y_scale,1])
		feet();
	{
		scale([x_scale,y_scale,1]) {
			female_pins();
			audio();
			buttons();
			power();
			usb();
			SD();
			WiFi();
			mic();
			capacitor();
			serial_male_pins();
			cspi();
		}
	}
}
// modules //
module cspi() {
	w = 7.3;
	l = 4.95;
	h = 8.65;
	y = 23.95;
	x = 62;
	translate([x - empty_space, y - empty_space, 0])
		cube([l + 2 * empty_space, w + 2 * empty_space, h]);
}
// serial male pins //
module serial_male_pins () {
	y = 25.65;
	x = 12;
	l = 2.5; // length
	w = 10; // width
	h = 10.4 - eh; // height
	translate([x - empty_space, y - empty_space, 0])
		cube([l + 2 * empty_space, w + 2 * empty_space, h]);
}
// capacitor //
module capacitor () {
	d = 6.3 + 2 * empty_space; // diameter
	h = 9.65 - eh; // height above epoxy
	x = 19.3 - 0.35 - d; // approx
	y = 45.75 - d;
	translate([x - empty_space, y - empty_space, 0])
	translate([d / 2, d / 2, 0])
		cylinder(d = d, h = h);
}
// microphone //
module mic () {
	/* rotations are around the base's center
	 * differencies are transfered and thus negated
	 * so, remember to re-add the part that was negated
	 * */
	d = 4.1 + 2 * empty_space; // diameter
	h = 3.3 - eh + empty_space; // height of mic
	a = 88; // angle
	translate([23.5 - d + empty_space, 1.5 - empty_space, 0])
		translate([d / 2, d / 2, 0]) {
			difference() {
				// remove the bottom part due to 
				// rotation going under
				z = d / cos(a); // intersection
//				translate([0, 0, h])
//				translate([0, (z - d) / 2, h])
//				echo (z , d, (z - d) / 2, (z - d) / 4);
//				translate([0, (z - d) / 2, 0])
				new_z = - d / 2 * sin(a); // make it glue at highest point of duct
				tan_a = sin(a) / cos(a);
				new_y = -new_z * tan_a; // while we put it lower, its y pos changes. This fixes it
				translate([0, 0, new_z + h]) // also put it above the mic (h)
					scale([1, cos(a), 1])
						translate([0, new_y, 0])
							rotate([a, 0, 0])
								cylinder(d = d, h = 200);
				translate([0, 0, h / 2])
					cube([2 * d, 2 * d, h], center = true);
			}
			cylinder(d = d, h = h);
		}
}
// antena WiFi //
module WiFi () {
	l = 10.95;
	w = 4.35;
	h = 4.4 - eh;
	translate([105.4 - l - empty_space, 52.45 - w - empty_space, 0])
		cube([l + 2 * empty_space,w + 2 * empty_space,h + empty_space]);
}
// micro SD //
module SD () {
	l = 14.1;
	w = 14.8;
	h = 3.5 - eh;
	translate([0,23.25 - w - empty_space,0])
		cube([l + empty_space,w + 2 * empty_space,h + empty_space]);
}
// usb micro //
module usb() {
	l = 5;
	w = 7.7;
	h = 4.5 - eh;
	translate([0,33.8 - w - empty_space,0])
		cube([l + empty_space, w + 2 * empty_space , h + empty_space]);
}
// power jack //
module power () {
	l = 10.85;
	w = 10.2;
	h = 9.3 - eh;
	translate([0,47.1 - w - empty_space,0])
		cube([l + empty_space, w + 2 * empty_space, h + empty_space]);
}
// 3x buttons //
module buttons () {
	lb = 7.9; // length big
	ls = 7;    // length short
	w = 2.85;
	hb = 5.2 - eh;
	hs = 2.6 - eh;
	translate ([10.35 - lb, 0, 0])
		button();
	translate ([102.45 - lb, 0, 0])
		button();
	translate([lb + (10.35 - lb),gw,0])
		rotate([0,0,180])
			button();
	// modules //
	module base () {
		translate([-empty_space, -empty_space, 0])
			cube([lb + 2 * empty_space, w + 2 * empty_space, hs + empty_space]);
	}
	module upper () {
		translate([-empty_space, -empty_space, 0])
			cube([ls + 2 * empty_space, w + 2 * empty_space, hb + empty_space]);
	}
	module button() {
		base ();
		translate ([(lb - ls) / 2 ,0 ,0])
			upper ();
	}
}
// audio //
module audio () {
	correction_l = 1;
	l = 12.15 + correction_l;
	w = 6.1;
	h = 6.95 - eh;
	module Cube(l,w,h) {
			translate([-empty_space, -empty_space, 0])
				cube([l + 2 * empty_space, w + 2 * empty_space, h + empty_space]);
	}
	translate([gl - l, 45.25 - w ,0])
		Cube(l,w,h);
	translate([gl - l, 35.1 - w ,0])
		Cube(l,w,h);
	translate([gl - l, 25 - w ,0])
		Cube(l,w,h);
	phh = 6.4 - eh;
	translate([gl - l, 14.3 - w ,0])
		Cube(l,w, phh);
}
// female pins //
module female_pins () {
	l_a = 47.5;
	w = 2.5;
	module Cube(l,w,h) {
			translate([-empty_space, -empty_space, 0])
				cube([l + 2 * empty_space, w + 2 * empty_space, h + empty_space]);
	}
	translate([65 - l_a, gw - 1.2 - w,0])
		Cube(l_a,w,sh);
	l_b = 23.5;
	translate([92.7 - l_b, gw - 1.2 - w, 0])
		Cube(l_b,w,sh);
	l_c = 38.4;
	translate([65 - l_c, 1.2,0])
		Cube(l_c,w,sh);
//	l = 23.5;
	translate([92.7 - l_b, 1.2, 0])
		Cube(l_b,w,sh);
}
// feet //
module feet() {
	fh = gh + 2;  // pillars will reach 2mm below pins
	fd = 3; // foot diameter (hole diameter) // fixed
	fbd = fd * 3; // base of cylinders (arbitrary)
	fbh = sh - ceiling_w - 0; // height of bases, 0mm above epoxy (arbitrary)
	bd = fd + 2 * 0.125; // ball diameter
	fpos_a =
	[
		[12.45, 49.2],
		[12.45, 0.95],
	];
	fpos_b =
	[
		[64.4, 34],
		[64.4, 6],
		[91.65, 34],
		[91.65, 6]
	];
	for (i = fpos_a) {
		foot(i[0], i[1]);
//		ball(i[0], i[1]);
	}
	for (i = fpos_b) {
		foot(i[0], i[1]);
		base(i[0], i[1]);
		ball(i[0], i[1]);
	}
	// modules //
	module foot(x,y) {
		translate([fd / 2, fd / 2, 0]) {
			translate([x, y, -(fh - sh)])
				cylinder(d =  fd,h = fh);
		}
	}
	module base(x, y) {
		translate([fd / 2, fd / 2, 0]) 
			translate([x, y, sh - ceiling_w])
				mirror([0,0,1])
					linear_extrude(height = fbh, scale = fd / fbd)
						circle(d = fbd);
	}
	module ball (x, y) {
		translate([fd / 2, fd / 2, 0])
			translate([x, y, -eh - 0.5])
				sphere(d = bd);
	}
}