$fs=1;
$fa=1;
/*
importance : 
height
radius
width
number
cw vs ccw
*/
wings = 5;
radius = 30;
height_half = 50;
blade_length = 15;
width = 3;
//                    //
// strengthening ring //
//                    //
module ring(R = radius, N = wings, CCW = true, W = width) {
	ring_h = 2;
	hole_d = 3.5; // souvlaki //
	ring_w = W*1.2;
	
	/// outer ring ///
	#linear_extrude(height = ring_h)
	difference() {
		circle(r = R + ring_w / 2);
		circle(r = R - ring_w / 2);
	}

	/// axis ///
	angle = asin((hole_d/2+ring_w/2)/R);
	cube_l = cos(angle)*R;
	
	for (i = [0:360/N:360])
		rotate([0,0,i])
		translate([radius,0,0])//attach to "blade"//
		rotate([0,0,180])//"flip" over y//
		rotate([0,0,angle * (CCW ? -1 : 1)])
		translate([0,-ring_w/2,0])//center y//
		color("yellow")
		cube([cube_l,ring_w,ring_h]);

	/// inner ring ///
	#linear_extrude(height = ring_h)
	difference() {
		circle(d = hole_d + 2*ring_w);
		circle(d = hole_d);
	}
}
ring();

/// change CCW for the top part ///
module blades(H = height_half, R = radius, L = blade_length, N = wings, CCW = true, W = width)
{
	//        //
	// blades //
	//        //
	step = 360 / N; // angle to next blade
	module wings() {
		linear_extrude(height = H, twist = step / 2) // height
		for (i = [0: step: 360]) {
			translate([R * cos(i), R * sin(i)]) // x, y coordinates
				rotate([0, 0, i + (CCW ? -90 : 90)])
					airfoil(L,W);
		}
	}
	
	difference() {
		#wings();
		holes();
	}
}
color("olive")
blades();

//       //
// holes //
//       //
module holes(H = height_half, R = radius, L = blade_length, N = wings, CCW = true, W = width) {
	step = 360 / N; // angle to next blade
	d = 2 * W / 3; // depth
	translate([0,0,H - d])
	for (i = [0 + 180: step: 360 + 180]) {
		translate([R * cos(i), R * sin(i), d / 2]) // x, y coordinates
			rotate([0, 0, i + (CCW ? -90 : 90)])
				cube([L / 3, W / 3, d] , center = true);
	}
}
//%#holes();

module hole_parts(R = radius, L = blade_length, N = wings, CCW = true, W = width) {
	new_d = 4 * W / 3; // depth
//		d = 2 * W / 3; // depth
	new_R = 5 + R;
//		translate([0,0,H - d])
	step = 360 / N; // angle to next blade
	for (i = [0: step: 360]) {
		translate([new_R * cos(i), new_R * sin(i), new_d / 2]) // x, y coordinates
//			translate([R * cos(i), R * sin(i), d / 2]) // x, y coordinates
			rotate([0, 0, i + (CCW ? -90 : 90)])
//					cube([L / 3, W / 3, new_d] , center = true);
				cube([L / 3-0.15, W / 3-0.15, new_d] , center = true);
	}
}
color("red") hole_parts();

// the following code is adapted from HooptieJ 
// @ https://www.thingiverse.com/thing:1250755/#files 
// and is licensed under the Creative Commons - Attribution - 
// - Non-Commercial license

module airfoil (length = 70, thickness = 3, detail = 27, open = false)
{
	// thickness is currently approximate
	// need to see what is naca or how it relates to thickness
	translate([-0.3*length,0])
	polygon(points = airfoil_data(thickness * 10 * 4 / 6, length, detail, open)); 
}
//

// this is the main function providing the airfoil data
function airfoil_data(naca=12, L = 100, N = 81, open = false) = 
  let(Na = len(naca)!=3?NACA(naca):naca)
  let(A = [.2969, -0.126, -.3516, .2843, open?-0.1015:-0.1036])
  [for (b=[-180:360/(N):179.99]) 
    let (x = (1-cos(b))/2)  
    let(yt = sign(b)*Na[2]/.2*(A*[sqrt(x), x, x*x, x*x*x, x*x*x*x])) 
    Na[0]==0?L*[x, yt]:L*camber(x, yt, Na[0], Na[1], sign(b))]; 

// helper functions
function NACA(naca) = 
  let (M = floor(naca/1000))
  let (P = floor((naca-M*1000)/100))
  [M/10, P/10, floor(naca-M*1000-P*100)/100];

function camber(x, y, M, P, upper) = 
  let(yc = (x<P)?M/P/P*(2*P*x-x*x): M/(1-P)/(1-P)*(1 - 2*P + 2*P*x -x*x))
  let(dy = (x<P)?2*M/P/P*(P-x):2*M/(1-P)/(1-P)*(P-x))
  let(th = atan(dy))
  [upper ? x - y*sin(th):x + y*sin(th), upper ? yc + y*cos(th):yc - y*cos(th)];