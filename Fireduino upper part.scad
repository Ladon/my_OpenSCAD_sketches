width=53.4;
length=108.8;
// the following are measured by hand(tool)
// holes    : 3mm
// width    : 53.4mm
// length   : 108.8mm
// hole perimeter to width, down : 1mm
// hole perimeter to width, up   : 1mm
// hole perimeter to length      : 12.3mm
// 4 holes, inner distance, width   : 24mm
// 4 holes, inner distance, height  : 24.85mm
// 4 holes, distance to bottom      : 6.05mm
// 4 holes, distance from top       : 16.25mm
// 4 holes, distance to right side  : 14mm
// 4 holes, distance to left side   : 64.5mm
// epoxy width  : 1.6mm
// pins approx  : 1.65mm (eye)



/*  
 * ~< H E I G H T s >~ (taken WITH epoxy height)
 * pins(colored)    : 10.05 <- 10.1 -> 10.25
 * sd card          : 3.55
 * power plug       : 9.05
 * usb              : 4.2 to 4.4
 * reset (button)   : 5.15
 * upgrade (button) : 5.1
 * power (button)   : 5.15
 * jtag (approx)    : 10.3
 * antenna          : 3.5
 * antenna (width)  : 10.15
 * 3x audio         : 6.85
 * 3x audio (width) : 6.15
 * 1x phone         : 6.35
 * 1x phone (width) : 6.55
 * capacitor        : 9.65
 */
/*
 * ~< P O S I T I O N s >~ (from bottom of epoxy)
 * sd card (approx) : 23.3
 * sd card width : 14.8
 * usb (approx)  : 33.85
 * usb width : 7.6
 * power : 47.15
 * power width : 10.2
 * spi : 31.6
 * spi width : 7.3
 * ~< P O S I T I O N s >~ (from left)
 * upgrade (button) : 9.9
 * upgrade (button) length : 7 to 7.75 extra=0.75
 * reset (button) (approx) : 10.05
 * reset (button) length : 6.9 to 7.75 extra=0.85
 * yellow upper pins : 65
 * yellow upper pins length : 47.7
 * jtag : 14.65
 * jtag length : 2.5
 * ~< P O S I T I O N s >~ (from right)
 * blue upper pins : 39.7
 * blue upper pins length : 23.5
 * antenna : 13.45
 * spi : 46.5
 * spi length : 5
 * ~< P O S I T I O N s >~ (from up)
 * yellow audio : 13.75
 * green audio : 23.85
 * pink audio : 34.1
 * antenna : 4
 * jtag : 27.7
 * jtag width : 9.8 (approx)
 * blue/yellow upper pins : 4 (approx)
 * capacitor : 14.15
 * capacitor diameter : 6.3
 * capaciror base diameter = +0.4? (approx) on each side
 * capacitor assuming base : 7.1x7.1
 * ~< P O S I T I O N s >~ (from bottom)
 * phone audio : 15.15
 * ~< P O S I T I O N s >~ (from right)
 * power (button) : 13.85
 * power (button) length : 7.05 to 7.75 extra=0.70
 * blue bottom pins : 39.6
 * blue bottom pins length : 23.5
 * ~< P O S I T I O N s >~ (from left)
 * yellow bottom pins : 65.05
 * yellow bottom pins length : 38.5
 */
 /*
  * Buttons overall :
  * 6.9 <- 7 <-> 7.05 -> 7.75
  * 5.1 <-> 5.15   - epoxy
  * 3 (arbitrary)
  */
epoxy_width=1.6;

capacitor_side=7.1;
capacitor_height=9.65-epoxy_width;
capacitor_x=18.35-6.3-0.4;
capacitor_y=width-14.15;
module capacitor() {
    color("LightGray")
    cube([capacitor_side,capacitor_side,capacitor_height]);
}

jtag_length=2.5;
jtag_x=14.65-jtag_length;
jtag_width=9.81;
jtag_y=width-27.7;
jtag_height=10.3-epoxy_width;
module jtag() {
    color("Gold")
    cube([jtag_length,jtag_width,jtag_height]);
}
spi_width=7.3;
spi_length=5;
spi_x=length-46.5;
spi_y=31.6-spi_width;
module spi() {
    color("Gold")
    cube([spi_length,spi_width,jtag_height]);
}
pin_width = 3;
pin_height = 10.25 - epoxy_width;
// ^ 3 thickness is arbitrary
yellow_bottom_pins_length=38.5;
yellow_bottom_pins_x=65.05-yellow_bottom_pins_length;
yellow_bottom_pins_y=4-pin_width;
module yellow_bottom_pins() {
    color("yellow")
    cube([yellow_bottom_pins_length,pin_width,pin_height]);
}
yellow_upper_pins_length=47.7;
yellow_upper_pins_x=65-yellow_upper_pins_length;
yellow_upper_pins_y=width-4;
module yellow_upper_pins() {
    color("yellow")
    cube([yellow_upper_pins_length,pin_width,pin_height]);
}
blue_bottom_pins_length=23.5;
blue_bottom_pins_x=length-39.6;
blue_bottom_pins_y=4-pin_width;
module blue_bottom_pins() {
    color("blue")
    cube([blue_bottom_pins_length,pin_width,pin_height]);
}
blue_upper_pins_length=23.5;
blue_upper_pins_x=length-39.7;
blue_upper_pins_y=width-4;
module blue_upper_pins() {
    color("blue")
    cube([blue_upper_pins_length,pin_width,pin_height]);
}

audio_length=13;
// ^ audio length is arbitrary
audio_height=6.85-epoxy_width;
audio_width=6.15;
audio_x=length-audio_length;
yellow_audio_y=width-13.75;
module yellow_audio() {
    color("yellow")
    cube([audio_length,audio_width,audio_height]);
}
green_audio_y=width-23.85;
module green_audio() {
    color("green")
    cube([audio_length,audio_width,audio_height]);
}
pink_audio_y=width-34.1;
module pink_audio() {
    color("pink")
    cube([audio_length,audio_width,audio_height]);
}

phone_height=6.35-epoxy_width;
phone_width=6.55;
phone_length=12.1;
phone_x=length-phone_length;
phone_y=15.15-phone_width;
module phone() {
    color("black")
    cube([phone_length,phone_width,phone_height]);
}

antenna_width=3.05;
antenna_height=3.5-epoxy_width;
antenna_length=10.15;
antenna_x=length-13.45;
antenna_y=width-4;
module antenna() {
    color("cyan")
    cube([antenna_length,antenna_width,antenna_height]);
}

/*
 * Buttons
 */
button_height=5.15-epoxy_width;
button_length=7.75;
// ^ with ground
button_width=2.75;
module button() {
    color("gray")
    cube([button_length,button_width,button_height]);
}
power_button_x=length-13.85-0.70/2;
// ^ 0.70/2 is one ground on the side
module power_button()
    button();
reset_button_x=10.05-6.9-0.85/2;
reset_button_y=width-button_width;
// ^ 0.85/2 is one ground on the side
module reset_button()
    button();
upgrade_button_x=9.9-7-0.75/2;
module upgrade_button()
    button();

sd_height=3.55-epoxy_width;
sd_width=14.8;
sd_length=14.05;
sd_y=23.3-sd_width;
module sd() {
    color("LightGray")
    cube([sd_length,sd_width,sd_height]);
}

usb_width=7.6;
usb_height=4.2-epoxy_width;
usb_length=5.05;
usb_y=33.85-usb_width;
module usb() {
    color("LightGray")
    cube([usb_length,usb_width,usb_height]);
}

power_width=10.2;
power_length=11.65;
power_height=9.05-epoxy_width;
power_y=47.15-power_width;
module power() {
    color("DimGray")
    cube([power_length,power_width,power_height]);
}

/*
 * Ceiling
 */
difference() {
    color("red")
    cube([length,width,pin_height],center=false);
    {
        // custom box for electronics and material ecomony :
        translate([usb_length,yellow_bottom_pins_y,0])
        cube([length-(usb_length+audio_length),width-(yellow_bottom_pins_y+(width-yellow_upper_pins_y-pin_width)),pin_height-1.5]);
        // bottom :
        translate([power_button_x,0,0])
        power_button();
        translate([upgrade_button_x,0,0])
        upgrade_button();
        translate([yellow_bottom_pins_x,yellow_bottom_pins_y,0])
        yellow_bottom_pins();
        translate([blue_bottom_pins_x,blue_bottom_pins_y,0])
        blue_bottom_pins();
        // leftmost :
        translate([0,sd_y,0])
        sd();
        translate([0,usb_y,0])
        usb();
        translate([0,power_y,0])
        power();
        // upper :
        translate([reset_button_x,reset_button_y,0])
        reset_button();
        translate([yellow_upper_pins_x,yellow_upper_pins_y,0])
        yellow_upper_pins();
        translate([blue_upper_pins_x,blue_upper_pins_y,0])
        blue_upper_pins();
        translate([antenna_x,antenna_y,0])
        antenna();
        // rightmost :
        translate([audio_x,yellow_audio_y,0])
        yellow_audio();
        translate([audio_x,green_audio_y,0])
        green_audio();
        translate([audio_x,pink_audio_y,0])
        pink_audio();
        translate([phone_x,phone_y,0])
        phone();
        // inner
        translate([spi_x,spi_y,0])
        spi();
        translate([jtag_x,jtag_y,0])
        jtag();
        translate([capacitor_x,capacitor_y,0])
        capacitor();
     }
}

/*
 * N E R V E s
 */
module cross(height,bottom,top) {
    color("blue") {
        linear_extrude(height=height,scale=[1,top/bottom])
//    translate([-1/2,-10/2,0])
        square([1,bottom],center=true);
        linear_extrude(height=height,scale=[top/bottom,1])
        square([bottom,1],center=true);
    }
}/*
 * Base (holes) :
 */
diameter=3;
// ^ hole
radius=diameter/2;
pins_below=1.65;
from_height=-(pins_below+epoxy_width);
cylinder_height=pins_below+epoxy_width+pin_height;
translate([0,0,from_height])
for(x = [
        [12.3+radius,1+radius,0],
        [12.3+radius,width-1-radius,0],
        [64.5+radius,6.05+radius,0],
        [64.5+radius,width-16.25-radius,0],
        [length-14-radius,6.05+radius,0],
        [length-14-radius,width-16.25-radius,0]
        ]){
//*/
///*
    translate(x+[0,0,(pins_below+epoxy_width)])
    cross(pin_height,diameter,diameter+2);
    translate(x) {
        difference() {
            ;
            color("yellow")
            cylinder(r=radius,h=cylinder_height,$fn=50);
        }
    }
}
//*/



/*
difference() {
*/
    /*
     * Base
     */
/*
    color("yellow")
    translate([-1,-1,-1])
    // ^ center is on actual epoxy board, not the cover's perimeter
    cube([length+2,width+2,epoxy_width+1+pins_below],center=false); 
    // ^ +1mm on each side
    color("blue") cube([length,width,epoxy_width+pins_below+10],center=false);
}
diameter=3;
// ^ hole
radius=1.5;
for(x = [
        [12.3+radius,1+radius,0],
        [12.3+radius,width-1-radius,0],
        [64.5+radius,6.05+radius,0],
        [64.5+radius,width-16.25-radius,0],
        [length-14-radius,6.05+radius,0],
        [length-14-radius,width-16.25-radius,0]
        ]){
*/
    /*
     * Base (holes)
     */
/*
    translate(x) {
        difference() {
            color("red")
            cylinder(r=radius+1,h=pins_below,$fn=50);
            // ^ 1mm foot for the board to stand
//           translate([0,0,pins_below])
            color("yellow")
            cylinder(r=radius,h=pins_below,$fn=50);
        }
    }
}
*/