// visual detail
$fn=100;

height = 8;

// filament hole
filament_hole_diameter = 8.8;
filament_hole_radius = filament_hole_diameter / 2;
filament_hole_height = height + 2;
filament_y = 1.3;
module filament() {
//    color("Olive")
        cylinder(d = filament_hole_diameter + 2.5,
            h = filament_hole_height, 
            center = true);
}
        
//outer perimeter
outer_diameter = 29 + 29;
outer_height = height + 1;
module outer() {
//    color("Yellow")
        cylinder(d = outer_diameter, 
            h = outer_height, 
            center = true);
}

module screws() {
    screw_hole_diameter = 3;
    screw_hole_radius = screw_hole_diameter / 2;
    screw_hole_height = height + 2;
    screw_hole_x_dist_centers = 17;
    screw_hole_y_dist_centers = 12;
    x = screw_hole_x_dist_centers / 2;
    y = screw_hole_y_dist_centers / 2;
    screw_places = [
        [-x,y],[-x,-y],
        [x,y],[x,-y]];
    // difference 0.45
    translate([0,- screw_hole_radius - 0.45 - (y - filament_hole_radius),0])
        for (i = screw_places) {
        translate(i)
            cylinder(d = screw_hole_diameter + 0.5, 
                h = screw_hole_height, 
                center = true);
        }
//    translate([0,filament_y,0])
//    translate([0, 0 ,0])
    filament();
}

// holders
holder_length = 42.75;
holder_diameter = height;
holder_radius = holder_diameter;
holder_y = 22;
holder_hole_diameter = 3;
ntiza_width = 7;
screw_head = 3;
screw_needs = 7;
module holder() {
    for(i = [0:360/3:359])
        rotate([0,0,i])
            translate([0,holder_y,0])
                rotate([0,90,0]) {
                    difference () {
                        cylinder(d = holder_diameter
                            ,h = holder_length
                            ,center = true);
                        cylinder(d = holder_hole_diameter + 0.3
                        ,h = holder_length + 2
                        ,center = true);
                        cylinder(d = holder_diameter + 1
                            ,h = holder_length 
                            - 2 * screw_needs
                            ,center = true);
                        echo(holder_length 
                            - 2 * screw_needs);
                    }

                    %cylinder(d = 9.9
                        ,h = holder_length 
                        + 2*ntiza_width 
                        + 2*screw_head
                        , center = true);
                }
}


module triangle() {
    triangle_height = height;
    triangle_length = holder_length;
    triangle_width = sqrt(pow(triangle_length,2) 
        - pow(triangle_length/2,2));
    tri_points = [[-triangle_length/2,0]
        ,[triangle_length/2,0]
        ,[0,triangle_width]];
    minus_this = tan(30)*triangle_length/2;
    linear_extrude(height = triangle_height, center = true)
        rotate([0,0,180])
            translate([0,-minus_this,0])
                polygon(tri_points);
    module cube_extra() {
        cube_y = minus_this;
        cube_width = holder_y - cube_y;
        cube_length = holder_length;
        cube_height = height;
        translate([0,cube_y + cube_width/2,0])
        cube([cube_length,cube_width,cube_height]
            ,center=true);
    }
    module cube_extra_minus(e = 3) {
        extra = e;
        cube_y = minus_this;
        cube_width = holder_y - cube_y - extra;
        cube_length = holder_length - 2 * screw_needs;
        cube_height = height + 1 ;
        translate([0,cube_y + cube_width/2 + extra,0])
        cube([cube_length,cube_width,cube_height]
            ,center=true);
    }
    for (i = [0:360/3:359])
        rotate([0,0,i])
            difference() {
                cube_extra();
                if (!i)
                    cube_extra_minus(0);
                else
                    cube_extra_minus(4);
                translate([0,holder_y,0])
                    rotate([0,90,0]) {
                        cylinder(d = holder_hole_diameter + 0.3
                        ,h = holder_length + 2
                        ,center = true);
                        cylinder(d = holder_diameter + 1
                            ,h = holder_length 
                            - 2 * screw_needs
                            ,center = true);
                    }
            }
}
scale([1.027,1.02,1]) {
    holder();
    difference() {
        triangle();
        {
            translate([0,0,0])
            screws();
            cyl_d = 27;
            cyl_r = cyl_d / 2;
            cyl_y = cyl_d / 2 + 7;
            translate([0,cyl_y,0])
                cylinder(d = cyl_d,h = height + 1, center = true);
    //        filament();
        }
    }
}
/*
horizontal lengths
    X 42.75
    Y 42.7
    Z 42.75

*/