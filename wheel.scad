$fs = 1;
$fa = 0.5;
radious = 20;
height = 4;
difference()
{
	cylinder(r = radious, h = height, center = true);
	//             //
	// large holes //
	//             //
	for (i = [0:360 / 3:360])
	{
		rotate([0,0,i])
		translate([radious / 2, 0, 0])
		cylinder(r = radious / 3, h = 2 * height, center = true);
	}
	//             //
	// small holes //
	//             //
	for (i = [0 + 360 / 6:360 / 3: 360 + 360 / 6])
	{
		rotate([0,0,i])
		translate([4 * radious / 6, 0, 0])
		cylinder(r = radious / 6, h = 2 * height, center = true);
	}
	axis();
}
module axis()
{
	//      //
	// axis //
	//      //
	axis_radious = 2;
	axis_sides_per_third = 2;
	axis_sides = axis_sides_per_third * 3;
	// shifting by the angle bisector (of a side) //
	phase = ((360 / 3) / 2) / axis_sides_per_third;
	linear_extrude(height = 2 * height, center = true)
	polygon(
	[
		for (i = [phase:360 / axis_sides:360+phase])
		[axis_radious * cos(i),axis_radious * sin(i)]
	]
	);
}
